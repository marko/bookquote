package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private static final Map<String, Double> prices = new HashMap<>(5);
    {
        prices.put("1", 10.0);
        prices.put("2", 45.0);
        prices.put("3", 20.0);
        prices.put("4", 35.0);
        prices.put("5", 50.0);
    }

    private static final double DEFAULT_PRICE = 0.0;
    public double getBookPrice(String s) {
        final var price = prices.get(s);
        return price == null ? DEFAULT_PRICE : price;
    }
}
